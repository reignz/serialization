import constants.Filepath;
import entity.Address;
import entity.User;
import service.SerializationService;
import service.impl.ByteArraySerializationServiceImpl;
import service.impl.ObjectSerializationServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Address address = new Address("Russia", "Kazan", "Glushko", 4, 123);
        User user = new User(1, "Regina", "regast", "112233", address);

        Address address1 = new Address("Russia", "Kazan", "Zorge", 5, 183);
        User user1 = new User(2, "Oleg", "oleg", "11223344", address1);

        List<User> users = new ArrayList<User>(Arrays.asList(user, user1));

        SerializationService serializationService1 = new ByteArraySerializationServiceImpl();
        SerializationService serializationService2 = new ObjectSerializationServiceImpl();

        serializationService1.serialize(users, Filepath.USERS);
        serializationService1.deserialize(Filepath.USERS);

        serializationService1.serialize(users, Filepath.USERS);
        serializationService2.deserialize(Filepath.USERS);
    }
}
