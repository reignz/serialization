package service.impl;

import entity.User;
import service.SerializationService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ByteArraySerializationServiceImpl implements SerializationService {

    public static ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    @Override
    public boolean serialize(List<User> list, String filePath) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

            for (User user : list) {
                objectOutputStream.writeObject(user);
            }
            objectOutputStream.flush();
            return true;
        } catch (IOException e) {
            return false;
        }
    }


    @Override
    public List<User> deserialize(String filePath) {

        List<User> list = new ArrayList<>();

        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(
                    new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
            list = ((ArrayList<User>)objectInputStream.readObject());

        } catch (Exception e) {

        }
        return list;
    }
}
