package service;

import entity.User;

import java.util.List;

public interface SerializationService {

    boolean serialize(List<User> list, String filePath);

    List<User> deserialize(String filePath);
}
