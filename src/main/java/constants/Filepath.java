package constants;

public interface Filepath {

    String RESOURCES = "src/main/resources/";
    String SERIALIZATION_DOCS  = RESOURCES + "serialization_docs/";
    String USERS = SERIALIZATION_DOCS + "users.out";
}
