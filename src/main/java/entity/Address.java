package entity;

import java.io.Serializable;

public class Address implements Serializable {

    private String county;
    private String city;
    private String avenue;
    private int houseNumber;
    private int apartmentNumber;

    public Address() {

    }

    public Address(String county, String city, String avenue, int houseNumber, int apartmentNumber) {
        this.county = county;
        this.city = city;
        this.avenue = avenue;
        this.houseNumber = houseNumber;
        this.apartmentNumber = apartmentNumber;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public int getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(int apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Address)) {
            return false;
        }
        Address a = (Address) o;
        return county.equals(a.county) &&
                city.equals(a.city) &&
                avenue.equals(a.avenue) &&
                Integer.valueOf(houseNumber).compareTo(Integer.valueOf(a.houseNumber)) == 0 &&
                Integer.valueOf(apartmentNumber).compareTo(Integer.valueOf(a.apartmentNumber)) == 0;
    }

    @Override
    public String toString() {
        return "Address{" +
                "country='" + county + '\'' +
                ", city='" + city + '\'' +
                ", avenue='" + avenue + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", apartmentNumber='" + apartmentNumber + '\'' +
                '}';
    }
}
